var libs = {
    node: require('/lib/xp/node'),
    repo: require('/lib/xp/repo'),
    context: require('/lib/xp/context'),
    cache: require('/lib/cache'),
    event: require('/lib/xp/event'),
    auth: require('/lib/xp/auth'),
    encoding: require('/lib/text-encoding')
};

//Increase cache size to a fair amount since unique cache keys are generated based on all parameters.
var repoCache = libs.cache.newCache({
    size: 42
});

var defaults = {
    repository: app.name,
    branch: 'master',
    principals: ['role:system.admin']
};

exports.repository = (function () {
    function create(params) {
        //create repo if it does not exist
        var repo = libs.repo.get(params.repository) || libs.repo.create({id: params.repository});
        //create branch if not exists
        var branchExists = repo.branches.indexOf(params.branch) !== -1;
        if (params.branch !== 'master' && !branchExists) {//master gets created as default
            log.debug('Create branch ' + params.branch + ' for repository ' + params.repository);
            libs.repo.createBranch({branchId: params.branch, repoId: params.repository});
        }
        var connectObj = {repoId: params.repository, branch: params.branch};
        if (params.user){
            connectObj.user = params.user;
        }
        if (params.principals){
            connectObj.principals = params.principals;
        }
        return libs.node.connect(connectObj);
    }

    return {
        getConnection: function (params) {
            //use all defaults if params is not set (when using only 'appersist.getConnection()')
            if (!params) {
                params = defaults
            }else{
                //if neither principals or user is set, use default
                if (!params.principals && !params.user){
                    params.principals = defaults.principals;
                }
                if (!params.branch){
                    params.branch = defaults.branch;
                }
                if (!params.repository){
                    params.repository = defaults.repository;
                }
            }

            return libs.context.run(params, function () {
                //Create a unique cachekey based on parameters
                var cacheKey = '';
                for(var key in params) {
                    var base64Text = libs.encoding.base64Encode(params[key].toString());
                    cacheKey += base64Text;
                }
                return repoCache.get(cacheKey, function () {
                    log.debug('Create new repository connection for ' + params.repository + ' branch ' + params.branch);
                    //TODO: Is there a security issue that connections get cached with user / principals
                    return create(params);
                });
            });
        }
    };
})();

libs.event.listener({
    type: 'repository.*', localOnly: false, callback: function (event) {
        //Cache will also be cleared when creating a new branch, because we cannot separate between delete / create in event.
        if (event.type === 'repository.deleted' || 'repository.updated'){
            repoCache.clear();
            log.debug('Cleared repository cache because of changes in repositories');
        }
    }
});

