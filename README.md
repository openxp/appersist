
![Scheme](docs/images/appersist-logo.png)

# Appersist library for Enonic XP

---
**Important build info**
Existing builds with dependency from maven url https://dl.bintray.com/openxp/public will break as bintray services are deprecated on May 1st 2021. Fix this by replacing maven url with following https://openxp.jfrog.io/artifactory/public.

---

A library to make it simpler to create your own node repositories that you can use in your app. Just use the command
appersist.repository.getConnection() which will return a [RepoConnection](http://repo.enonic.com/public/com/enonic/xp/docs/6.13.0/docs-6.13.0-libdoc.zip!/module-node-RepoConnection.html). 
See example use below.


## Note! On clustered Enonic XP setups

Remember when initializing stuff, for example in main.js inside an app, use the require('/lib/xp/cluster').isMaster() in the [Cluster API](https://developer.enonic.com/docs/xp/stable/api/lib-cluster) to make sure stuff is initialized only once, not once _for each node_ in the cluster.

## How to include in Enonic XP projects build.gradle

    repositories {
        maven {
            url 'https://openxp.jfrog.io/artifactory/public'
        }
    }
    dependencies {
        include "openxp.lib:appersist:2.1.0"
    }


## Releases and Compatibility
See [info about releases on Enonic Market](https://market.enonic.com/vendors/rune-forberg/appersist)

2.1.0 - Upgraded Enonic XP version 7.3.2 to get new api methods of RepoConnection  
XP7 Update 1: New exists() function for both node and content API provides faster existence checks over using get()  
XP7 Update 2: Retrieve an old version of a node by using the RepoConnection::get method  

## Example use

In your applications src/main/resources/main.js

* Will create a new content repository with [app.name] as name and branch master on application startup if it does not exist, and return connection

```javascript
    var appersist = require('/lib/openxp/appersist');
    appersist.repository.getConnection();
```

* Will re-use existing connection and create a new content

```javascript
    const clusterLib = require('/lib/xp/cluster');
    var appersist = require('/lib/openxp/appersist');
    if (clusterLib.isMaster()){
        appersist.repository.getConnection().create({favouriteFish:'Napoleon Fish'});
    }
```

* Will create a new repository 'fish' with branch 'draft' and create a new content there

```javascript
    const clusterLib = require('/lib/xp/cluster');
    var appersist = require('/lib/openxp/appersist');
    if (clusterLib.isMaster()){
        appersist.repository.getConnection({repository:'fish', branch:'draft'}).create({favouriteFish:'Napoleon Fish'});
    }
```

## Defaults / parameters

The getConnection() parameters defaults to the object below, but can be overridden with same format as the [Enonic XP context library parameters](http://repo.enonic.com/public/com/enonic/xp/docs/6.13.0/docs-6.13.0-libdoc.zip!/module-context.html#.run)
repository, branch, user, principals can be sendt in as parameters. If principals or user parameter is sendt in, it will override default principals.

Defaults:

```javascript
    var defaults = {
        repository: app.name,
        branch: 'master',
        principals: ['role:system.admin']
    };
```